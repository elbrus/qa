#!/usr/bin/perl

# Copyright (C) 2009  Christoph Berg <myon@debian.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

use warnings;
use strict;
use DBD::Pg;

my $verbose = shift @ARGV;

# prepare projectb

my $projectb = DBI->connect ("dbi:Pg:service=dak", "", "",
	{AutoCommit => 0, RaiseError => 1, PrintError => 1});

my $p = $projectb->prepare ("SELECT source AS package, version, install_date,
	changer.name AS changer,
	uid.name || ' <' ||
		CASE WHEN uid.uid ~ '\@' THEN uid.uid ELSE uid.uid || '\@debian.org' END || '>' AS signer
FROM source
	JOIN maintainer changer ON (source.changedby = changer.id)
	JOIN fingerprint ON (source.sig_fpr = fingerprint.id)
	JOIN uid ON (fingerprint.uid = uid.id)
WHERE (source, version) = (?, ?)
LIMIT 1");

# write to qadb

my $qadb = DBI->connect ("dbi:Pg:service=qa", "", "",
	{AutoCommit => 0, RaiseError => 1, PrintError => 1});

my $update_q = $qadb->prepare ("UPDATE source SET
	changed_by = maint_id_or_new (?),
	signed_by = maint_id_or_new (?),
	date = ?
	WHERE package_id = ?");

my $q = $qadb->prepare ("SELECT package_id, package, version
FROM source
	JOIN package USING (package_id)
	JOIN packagelist USING (package_id)
	JOIN suite USING (suite_id)
WHERE changed_by IS NULL
	AND archive = 'debian'");
$q->execute;

while (my $row = $q->fetchrow_hashref) {
	print "Looking for $row->{package} $row->{version}\n" if ($verbose);
	$p->execute ($row->{package}, $row->{version});
	my $data = $p->fetchrow_hashref;
	unless ($data) {
		print "  $row->{package} $row->{version} not found in projectb\n" if ($verbose);
		next;
	}
	$update_q->execute ($data->{changer}, $data->{signer}, $data->{install_date},
		$row->{package_id});
}

$qadb->commit;
$qadb->disconnect;

$projectb->rollback;
$projectb->disconnect;
