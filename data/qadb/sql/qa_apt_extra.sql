-- this should be empty:
SELECT * FROM package p WHERE NOT EXISTS (SELECT * FROM package_control pc WHERE p.package_id = pc.package_id);

-- find missing source references
INSERT INTO package_source
SELECT missing.package_id, s.package_id AS source_id FROM
        (SELECT p.*,
                substring(pc.control FROM E'Source: ([^ \\n]*)') AS source_package,
                substring(pc.control FROM E'Source: [^ \\n]* \\(([^ \\n]*)\\)') AS source_version
        FROM package p
                JOIN package_control pc USING (package_id)
        WHERE pkg_architecture <> 'source' AND
                NOT EXISTS (SELECT * FROM package_source ps WHERE ps.package_id = p.package_id))
        AS missing
JOIN package s
        ON (s.package = COALESCE(missing.source_package, missing.package)
                AND s.version = COALESCE(missing.source_version, regexp_replace(missing.version, E'\\+b\\d+$', ''))
                AND s.pkg_architecture = 'source');

-- find missing 'source' rows
INSERT INTO source (package_id, maintainer, section, priority, dm_upload_allowed)
SELECT package_id,
	maint_id_or_new(substring(pc.control FROM E'Maintainer: ([^\\n]*)')) AS maintainer,
	substring(pc.control FROM E'Section: ([^\\n]*)') AS section,
	substring(pc.control FROM E'Priority: ([^\\n]*)') AS priority,
	coalesce(substring(pc.control FROM E'Dm-Upload-Allowed: ([^\\n]*)'), 'no') = 'yes' AS dm_upload_allowed
FROM package p
	JOIN package_control pc USING (package_id)
WHERE pkg_architecture = 'source' AND
	NOT EXISTS (SELECT * FROM source s WHERE s.package_id = p.package_id);

-- find missing 'uploader' rows (PG >= 8.3)
-- FIXME: ERROR:  duplicate key value violates unique constraint "uploader_pkey"
INSERT INTO uploader
SELECT package_id, maint_id_or_new(uploader[1])
FROM
(SELECT package_id,
	regexp_matches (uploaders, E'[^,@ ][^@]+@[^@]+>', 'g') AS uploader
FROM
	(SELECT package_id,
		substring(pc.control FROM E'Uploaders: ([^\\n]*)') AS uploaders
	FROM package p
		JOIN package_control pc USING (package_id)
	WHERE pkg_architecture = 'source' AND
		NOT EXISTS (SELECT * FROM uploader u WHERE u.package_id = p.package_id) AND
		substring(pc.control FROM E'Uploaders: ([^\\n]*)') IS NOT NULL)
	AS uploaders)
AS uploader;

-- find missing package information
INSERT INTO package_info
SELECT package_id, 'Description' AS field, value FROM
        (SELECT p.*,
                substring(pc.control FROM E'Description: ([^\\n]*)') AS value
        FROM package p
                JOIN package_control pc USING (package_id)
        WHERE pkg_architecture <> 'source' AND
                NOT EXISTS (SELECT * FROM package_info pi WHERE pi.package_id = p.package_id AND pi.field = 'Description'))
        AS missing;
INSERT INTO package_info
SELECT package_id, 'Section' AS field, value FROM
        (SELECT p.*,
                substring(pc.control FROM E'Section: ([^\\n]*)') AS value
        FROM package p
                JOIN package_control pc USING (package_id)
        WHERE pkg_architecture <> 'source' AND
                NOT EXISTS (SELECT * FROM package_info pi WHERE pi.package_id = p.package_id AND pi.field = 'Section'))
        AS missing;
INSERT INTO package_info
SELECT package_id, 'Priority' AS field, value FROM
        (SELECT p.*,
                substring(pc.control FROM E'Priority: ([^\\n]*)') AS value
        FROM package p
                JOIN package_control pc USING (package_id)
        WHERE pkg_architecture <> 'source' AND
                NOT EXISTS (SELECT * FROM package_info pi WHERE pi.package_id = p.package_id AND pi.field = 'Priority'))
        AS missing
WHERE value IS NOT NULL;
