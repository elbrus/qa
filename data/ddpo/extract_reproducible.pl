#!/usr/bin/perl

# https://tests.reproducible-builds.org/debian/reproducible-tracker.json

use strict;
use warnings;
use JSON;
use DB_File;

undef $/; # slurp mode
my $js = <>;
my $ci = decode_json ($js);

my $db_filename = 'reproducible-new.db';
my %db;
my $db_file = tie %db, "DB_File", $db_filename, O_RDWR|O_CREAT|O_TRUNC, 0666, $DB_BTREE
    or die "Can't open database $db_filename : $!";

# [
#     {
#         "architecture_details": [
#             {
#                 "architecture": "amd64",
#                 "build_date": "2015-11-25 03:10",
#                 "status": "reproducible",
#                 "version": "1.2.2-1"
#             }
#         ],
#         "package": "referencer",
#         "status": "reproducible",
#         "suite": "unstable",
#         "version": "1.2.2-1"
#     },

foreach my $pkg (@$ci) {
	# we are ignoring suite and architecture
	$db{"status:$pkg->{package}"} = $pkg->{status};
	$db{"version:$pkg->{package}"} = $pkg->{version};
}
