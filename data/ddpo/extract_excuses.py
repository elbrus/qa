#! /usr/bin/env python
#  Copyright (C) 2002 Igor Genibel
#  Copyright (C) 2006 Christoph Berg <myon@debian.org>
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.

import sys, os, re, urllib, dbm, signal, ssl

class TimeoutError(Exception):
    pass

def alarmHandler(*args):
    """
    signal handler for SIGALRM, just raise an exception
    """
    raise TimeoutError

def extract_excuses():
    excuses_db = dbm.open("excuses-new", 'c')
    signal.signal(signal.SIGALRM, alarmHandler)
    try:
        # set timeout
        signal.alarm(10)
        debian_ca_path = '/etc/ssl/ca-debian'
        if os.path.isdir(debian_ca_path):
            context = ssl.create_default_context(capath=debian_ca_path)
            excuse_file = urllib.urlopen("https://release.debian.org/britney/update_excuses.html", context=context)
        else:
            excuse_file = urllib.urlopen("https://release.debian.org/britney/update_excuses.html")
        signal.alarm(0)
    except "TimeOut":
        print >> sys.stderr,  "[WARNING: skipping testing-excuses due to ftp-master timeout!"
        signal.alarm(0)
        return

    re_id = re.compile(r'^<li><a id="(.+?)"\s.+\((.+?)\sto.+') 
    re_ul = re.compile(r'</ul>')
    re_dep = re.compile(r'<li>Depends: .+ <a href="#(.+?)"')
    re_maint = re.compile(r'<li>Maintainer:')
    re_dash = re.compile(r'^-.+')
    re_slash = re.compile(r'.+/.+')
    re_less = re.compile(r'<=')
    re_greater = re.compile(r'> 0')
    re_target = re.compile(r' target="_blank"')

    opened = 0
    package = ""
    excuse = ""
    valid = 0

    while 1:
        line = excuse_file.readline()
        if line == "": break
        match_id = re_id.search(line)
        match_ul = re_ul.search(line)
        match_dep = re_dep.search(line)
        if re_maint.match(line):
            continue

        if match_id:
            package = match_id.group(1)
            match_dash = re_dash.search(package)
            if match_dash:
                continue
            match_slash = re_slash.search(package)
            if match_slash:
                continue
            version = match_id.group(2)
            excuse = "<!-- Testing_version: "+version+" -->\n"
            excuse += "<h2>Excuse for "+package+"</h2>\n"
            opened = 1
            valid = 1

        elif match_ul and (valid == 1):
            excuse += '</ul>'
            excuses_db[package] = excuse
            package = ""
            opened = 0
            valid = 0

        elif match_dep and (valid == 1):
            link = match_dep.group(1)
            excuse += '<li>Depends: '+package+' <a href="developer.php?excuse='+link+'">'+link+'</a></li>\n'

        elif (opened == 1) and (valid == 1):
            line = re_less.sub('&lt;=', line)
            line = re_greater.sub('&gt; 0', line)
            line = re_target.sub('', line)
            excuse += line

    excuses_db.close()
    return

extract_excuses()

# vim:sw=4:softtabstop=4:expandtab
